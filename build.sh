#! /bin/bash
set -v
IP=$(traceroute -n google.com |
     egrep -v ' (10|172\.(1[6-9]|2[0-9]|3[01])|192.168)\.' |
     awk '/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+.*ms/ {print $2; exit}')
echo Found IP: $IP
cat << EOF > smokeping/config.d/Targets
*** Targets ***
probe = FPing

menu = Top
title = Network Latency Graph
remark = Welcome.

+ Inet
menu = Inet
title = Inet Network

++ Google
menu = Google
title = Google
host = www.google.com

+ Local
menu = Local
title = Local Network

++ OnHub
menu = OnHub
title = OnHub router
host = onhub.here

++ ISP
menu = ISP
title = ISP gateway ($IP)
host = $IP

++ DNS
menu = DNS
title = Domain Name Server (8.8.8.8)
host = 8.8.8.8

++ Modem
menu = Modem
title = ADSL modem
host = 192.168.1.254
EOF
sudo docker build -t filmar-smokeping .


