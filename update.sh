#! /bin/bash
# Updates the smokeping configuration.
set -v
./build.sh
sudo docker rm -f smokeping
sudo docker run -it --name smokeping -p 18080:80 \
	-d filmar-smokeping \
	-e filmil@gmail.com \
	-T PST8PDT \
	-o "Filmar family"
