# My smokeping docker configuration

This is my custom smokeping docker configuration for measuring ping times
for a few hosts that I care about from my home network.

The setup is based on the container `dperson/smokeping`.

By default, the report is accessible at:

http://hostname:18080/smokeping/smokeping.cgi
